var path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
  plugins: [
    new MiniCssExtractPlugin({
      filename: "style-mono.css"
    })
  ],
  entry: [
    "./src/index.styl",
    "./src/index.js"
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "mono.js",
    publicPath: "/dist"
  },
  module: {
    rules: [
      {
        test: /\.styl$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: { hmr: false }
          },
          { loader: "css-loader?-url" },
          { loader: "stylus-loader" }
        ]
      }
    ]
  }
};
